package JavaCore;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.stream.Collectors;

public class BaiTap19 {
    public static void main(String[] args) {
        Scanner scanner;
        boolean isInputDone = false;
        String option = null;
        long a,b,c;
        do{
            System.out.println("Chung toi ho tro giai 2 loai phuong trinh:");
            System.out.println("a) Giai phuong trinh bac 1 (ax + b = 0)");
            System.out.println("b) Giai phuong trinh bac 2 (ax2 + bx + c = 0)");
            System.out.print("Nhap option ban muon(a,b): ");
            scanner = new Scanner(System.in);
            option = scanner.nextLine();
            String[] options = {"a","b"};
            String finalOption = option;
            if (Arrays.stream(options).filter(opt -> finalOption.equals(opt)).collect(Collectors.toList()).size() == 0) {
                System.out.println("Xin nhap lai dung ky tu muc!!!!");
                continue;
            }
            isInputDone = true;
        } while (!isInputDone);


        GiaiPhuongTrinhBac1:
        {
            if(!option.equals("a")) break GiaiPhuongTrinhBac1;
            System.out.print("Nhap a: ");
            scanner = new Scanner(System.in);
            a = scanner.nextLong();
            System.out.print("Nhap b: ");
            scanner = new Scanner(System.in);
            b = scanner.nextLong();
            System.out.println("x = " + (-b/a));
        }

        GiaiPhuongTrinhBac2:
        {
            if(!option.equals("b")) break GiaiPhuongTrinhBac2;
            System.out.print("Nhap a: ");
            scanner = new Scanner(System.in);
            a = scanner.nextLong();
            System.out.print("Nhap b: ");
            scanner = new Scanner(System.in);
            b = scanner.nextLong();
            System.out.print("Nhap c: ");
            scanner = new Scanner(System.in);
            c = scanner.nextLong();
            float delta = b*b - 4*a*c;
            if(delta < 0){
                System.out.println("Phuong trinh vo nghiem");
            } else if(delta == 0){
                System.out.println("Phuong trinh co nghiem kep x1=x2= " + (-b/(2*a)));
            } else {
                System.out.println("x1 = " + (-b + Math.sqrt(delta))/(2*a));
                System.out.println("x2 = " + (-b - Math.sqrt(delta))/(2*a));
            }
        }
    }
}
