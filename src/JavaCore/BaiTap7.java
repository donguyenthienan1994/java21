package JavaCore;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BaiTap7 {
    public static void main(String[] args) {
        Scanner scanner = null;
        long number = 0;
        boolean isInputDone = false;
        do{
            try {
                System.out.print("Nhap so tu nhien: ");
                scanner = new Scanner(System.in);
                number = scanner.nextLong();
                isInputDone = true;
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so");
                continue;
            }
        } while (!isInputDone);

        System.out.printf("Cac boi so cua %d la: " , number);
        for (int i = 1; i < number; i++) {
            if(number % i == 0) System.out.print(i + " ");
        }
    }
}
