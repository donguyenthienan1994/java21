package JavaCore;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BaiTap10 {
    public static void main(String[] args) {
        Scanner scanner = null;
        long number = 0;
        boolean isInputDone = false;
        do{
            try {
                System.out.print("Nhap so tu nhien: ");
                scanner = new Scanner(System.in);
                number = scanner.nextLong();
                isInputDone = true;
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so");
                continue;
            }
        } while (!isInputDone);

        long sum = 0;

        for (int i = 1; i <= number; i++) {
            boolean isSoNgTo = false;
            int count = 0;
            for (int j = 1; j <= i; j++) {
                if(i % j == 0) count++;
            }
            if(count <= 2) isSoNgTo = true;
            if (isSoNgTo) sum = sum + i;
        }

        System.out.printf("Tong cac so nguyen to tu 1 toi %d la %d", number, sum);
    }
}
