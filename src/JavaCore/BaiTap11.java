package JavaCore;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BaiTap11 {
    public static void main(String[] args) {
        Scanner scanner = null;
        long number = 0;
        boolean isInputDone = false;
        do{
            try {
                System.out.print("Nhap so tu nhien: ");
                scanner = new Scanner(System.in);
                number = scanner.nextLong();
                isInputDone = true;
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so");
                continue;
            }
        } while (!isInputDone);

        int log2 = (int)(Math.log(number) / Math.log(2));
        int max = 0;
        for (int i = 0; i < number; i++) {
            if(i > log2){
                max = i - 1;
                break;
            }
        }
        if(max == 0)
            System.out.printf("Khong co so tu nhien lon nhat nho hon log2(%d) la %d", number, max);
        else System.out.printf("So tu nhien lon nhat nho hon log2(%d) la %d", number, max);
    }

}
