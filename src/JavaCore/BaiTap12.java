package JavaCore;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BaiTap12 {
    public static void main(String[] args) {
        Scanner scanner = null;
        byte number = 0;
        boolean isInputDone = false;
        do{
            try {
                System.out.print("Nhap so tu nhien tu 1-9: ");
                scanner = new Scanner(System.in);
                number = scanner.nextByte();
                if(number > 9 || number < 1) throw new InputMismatchException();
                isInputDone = true;
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so tu nhien tu 1-9");
                continue;
            }
        } while (!isInputDone);

        System.out.printf("Tam giac vuong tuong ung voi so tu nhien %d la : \n", number);

        for (int i = 1; i <= number; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j);
            }
            System.out.print("\n");
        }
    }

}
