package JavaCore;

import java.util.*;
import java.util.stream.Collectors;

public class BaiTap15 {
    public static void main(String[] args) {
        Scanner scanner = null;
        System.out.print("Nhap chuoi ky tu: ");
        scanner = new Scanner(System.in);
        String str = scanner.nextLine();

        System.out.printf("Do dai cua chuoi '%s' la %d \n", str, str.length());
        System.out.print("Nhap index ban muon truy xuat: ");
        scanner = new Scanner(System.in);
        int index = scanner.nextInt();
        System.out.printf("Ky tu tai index '%d' la '%s'\n",index, str.charAt(index));
        String subStr = "abcdef";
        if(str.contains(subStr))
            System.out.printf("Chuoi '%s' co chua chuoi phu '%s'", str, subStr);
        else System.out.printf("Chuoi '%s' khong chua chuoi phu '%s'", str, subStr);
    }
}
