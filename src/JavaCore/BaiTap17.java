package JavaCore;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class BaiTap17 {
    public static void main(String[] args) {
        Random random = new Random();
        boolean isCheckingDone = false;
        int randomNum = random.nextInt(1000);
        //Nhap so can check
        do{
            try {
                System.out.print("Nhap so muon kiem tra: ");
                Scanner scanner = new Scanner(System.in);
                int num = scanner.nextInt();
                if(num > randomNum){
                    System.out.println(" So ban nhap dang lon hon");
                    continue;
                } else if(num < randomNum){
                    System.out.println(" So ban nhap dang nho hon");
                    continue;
                } else isCheckingDone = true;
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so chinh xac");
                continue;
            }
        } while (!isCheckingDone);

        System.out.println("Xin chuc mung ban da nhap dung so");

    }
}
