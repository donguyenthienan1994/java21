package JavaCore;

import java.util.*;
import java.util.stream.Collectors;

public class BaiTap14 {
    public static void main(String[] args) {
        Scanner scanner = null;
        int arrLength = 0;
        boolean isInputDone = false;
        //Nhap chieu dai mang
        do{
            try {
                System.out.print("Nhap so phan tu cua mang: ");
                scanner = new Scanner(System.in);
                arrLength = scanner.nextInt();
                isInputDone = true;
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so chinh xac");
                continue;
            }
        } while (!isInputDone);

        int[] arr = new int[arrLength];

        //Nhap gia tri tung thanh phan trong mang
        for (int i = 0; i < arrLength; i++) {
            isInputDone = false;
            do{
                try {
                    System.out.printf("Nhap gia tri phan tu thu %d cua mang: ", i);
                    scanner = new Scanner(System.in);
                    int value = scanner.nextInt();
                    arr[i] = value;
                    isInputDone = true;
                } catch (InputMismatchException e){
                    System.out.println("Xin hay nhap so chinh xac");
                    continue;
                }
            } while (!isInputDone);
        }

        Set<Integer> set = new HashSet<>(Arrays.stream(arr).boxed().collect(Collectors.toList()));
        List list = new ArrayList<>(set);
        System.out.print("Mang sau khi xoa cac phan tu bi lap lai: " + list);
    }
}
