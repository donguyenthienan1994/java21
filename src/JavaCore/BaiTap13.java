package JavaCore;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BaiTap13 {
    public static void main(String[] args) {
        Scanner scanner = null;
        int arrLength = 0;
        boolean isInputDone = false;
        //Nhap chieu dai mang
        do{
            try {
                System.out.print("Nhap so phan tu cua mang: ");
                scanner = new Scanner(System.in);
                arrLength = scanner.nextInt();
                isInputDone = true;
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so chinh xac");
                continue;
            }
        } while (!isInputDone);

        int[] arr = new int[arrLength];

        //Nhap gia tri tung thanh phan trong mang
        for (int i = 0; i < arrLength; i++) {
            isInputDone = false;
            do{
                try {
                    System.out.printf("Nhap gia tri phan tu thu %d cua mang: ", i);
                    scanner = new Scanner(System.in);
                    int value = scanner.nextInt();
                    arr[i] = value;
                    isInputDone = true;
                } catch (InputMismatchException e){
                    System.out.println("Xin hay nhap so chinh xac");
                    continue;
                }
            } while (!isInputDone);
        }

        System.out.println("Please choose 1 one these options:");
        System.out.println("a) Tinh gia tri trung binh");
        System.out.println("b) Tinh phan tu lon nhat nho nhat");
        System.out.println("c) Tim phan tu am lon nhat nho nhat trong mang");
        System.out.println("d) Tim phan tu duong lon nhat nho nhat trong mang");
        System.out.println("e) In ra cac phan tu chan le trong mang");
        System.out.println("f) Them 1 phan tu theo index");
        System.out.println("g) Xoa 1 phan tu theo index");

        isInputDone = false;
        String option = null;
        do{
            System.out.print("Nhap option ban muon: ");
            scanner = new Scanner(System.in);
            option = scanner.nextLine();
            String[] options = {"a","b","c","d","e","f","g","h"};
            String finalOption = option;
            if (Arrays.stream(options).filter(opt -> finalOption.equals(opt)).collect(Collectors.toList()).size() == 0) {
                System.out.println("Xin nhap lai dung ky tu muc!!!!");
                continue;
            }
            isInputDone = true;
        } while (!isInputDone);


        TinhGiaTriTrungBinh:
        {
            if(!option.equals("a")) break TinhGiaTriTrungBinh;
            float sum = 0;
            for (int i = 0; i < arrLength; i++) {
                sum = sum + arr[i];
            }
            System.out.printf("Gia tri trung binh cua mang la: %f", sum / arrLength);
        }


        TimMinMax:
        {
            if(!option.equals("b")) break TimMinMax;
            int max = Arrays.stream(arr).max().getAsInt();
            int min = Arrays.stream(arr).min().getAsInt();
            System.out.println("Gia tri nho nhat la : " + min);
            System.out.println("Gia tri lon nhat la : " + max);
        }

        TimMinMaxMinus:
        {
            if(!option.equals("c")) break TimMinMaxMinus;
            try {
                int max = Arrays.stream(arr).filter(item -> item < 0).max().getAsInt();
                int min = Arrays.stream(arr).filter(item -> item < 0).min().getAsInt();
                System.out.println("Gia tri am nho nhat la : " + min);
                System.out.println("Gia tri am lon nhat la : " + max);
            }catch (NoSuchElementException e){
                System.out.println("Khong co so am nao trong mang ca!!!");
            }
        }

        TimMinMaxPos:
        {
            if(!option.equals("d")) break TimMinMaxPos;
            try {
                int max = Arrays.stream(arr).filter(item -> item > 0).max().getAsInt();
                int min = Arrays.stream(arr).filter(item -> item > 0).min().getAsInt();
                System.out.println("Gia tri duong nho nhat la : " + min);
                System.out.println("Gia tri duong lon nhat la : " + max);
            }catch (NoSuchElementException e){
                System.out.println("Khong co so duong nao trong mang ca!!!");
            }
        }

        TimOddEven:
        {
            if(!option.equals("e")) break TimOddEven;
            int[] odd = Arrays.stream(arr).filter(item -> item % 2 == 0).toArray();
            int[] even = Arrays.stream(arr).filter(item -> item % 2 == 1).toArray();
            System.out.println("Gia tri chan trong mang la : " + Arrays.toString(odd));
            System.out.println("Gia tri le trong mang la : " + Arrays.toString(even));
        }

        AddPhanTuTheoIndex:
        {
            if(!option.equals("f")) break AddPhanTuTheoIndex;
            isInputDone = false;
            int index = -1, value = -1;
            do{
                try {
                    System.out.print("Nhap index can add: ");
                    scanner = new Scanner(System.in);
                    index = scanner.nextInt();
                    System.out.println("Nhap gia tri can add: ");
                    scanner = new Scanner(System.in);
                    value = scanner.nextInt();
                    isInputDone = true;
                } catch (InputMismatchException e){
                    System.out.println("Xin hay nhap chinh xac index va gia tri can add");
                    continue;
                }
            } while (!isInputDone);
            List<Integer> finalList = Arrays.stream(arr).boxed().collect(Collectors.toList());
            finalList.add(index, value);
            System.out.println("Mang sau khi duoc add la " + finalList);
        }

        RemovePhanTuTheoIndex:
        {
            if(!option.equals("g")) break RemovePhanTuTheoIndex;
            isInputDone = false;
            int index = -1;
            do{
                try {
                    System.out.print("Nhap index can xoa: ");
                    scanner = new Scanner(System.in);
                    index = scanner.nextInt();
                    isInputDone = true;
                } catch (InputMismatchException e){
                    System.out.println("Xin hay nhap chinh xac index can remove");
                    continue;
                }
            } while (!isInputDone);
            List<Integer> finalList = Arrays.stream(arr).boxed().collect(Collectors.toList());
            finalList.remove(index);
            System.out.println("Mang sau khi duoc xoa la " + finalList);
        }
    }
}
