package JavaCore;

import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BaiTap16 {
    public static void main(String[] args) {
        boolean isInputDone = false;
        int year = 0;
        //Nhap so nam
        do{
            try {
                System.out.print("Nhap so nam muon kiem tra: ");
                Scanner scanner = new Scanner(System.in);
                year = scanner.nextInt();
                isInputDone = true;
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so nam chinh xac");
                continue;
            }
        } while (!isInputDone);

        if(year % 4 == 0)
            System.out.printf("Nam '%d' la nam nhuan", year);
        else System.out.printf("Nam '%d' khong la nam nhuan", year);
    }
}
