package JavaCore;

import java.util.*;

public class BaiTap21 {
    public static void main(String[] args) {
        Scanner scanner = null;
        Map<String, Integer> map = new HashMap<>();
        boolean isInputDone = false;
        int x1,x2,v1,v2;
        do{
            System.out.println("Xin nhap thong tin theo dieu kien sau:");
            System.out.println("Điều kiện: 0 <= x1 < x2 < 10000; 1 <= v1, v2 <= 10000");
            for (int i = 1; i <= 2; i++) {
                //Nhap vi tri va van toc
                do{
                    try {
                        System.out.printf("Nhap vi tri kangaroo %d: ", i);
                        scanner = new Scanner(System.in);
                        map.put(String.format("x%d", i), scanner.nextInt());
                        System.out.printf("Nhap van toc kangaroo %d: ", i);
                        scanner = new Scanner(System.in);
                        map.put(String.format("v%d", i), scanner.nextInt());
                        isInputDone = true;
                    } catch (InputMismatchException e){
                        System.out.println("Xin hay nhap so chinh xac");
                        continue;
                    }
                } while (!isInputDone);
            }
            x1 = map.get("x1");
            x2 = map.get("x2");
            v1 = map.get("v1");
            v2 = map.get("v2");
        } while (!(x1 >= 1 && x1 < x2 && x1 <= 10000 && x2 <= 10000 && v1 >= 1 && v2 >= 1 && v1 <= 10000 && v2 <= 10000));

        if (v2 < v1){
            System.out.println("Hai kangaroo se gap nhau");
        } else System.out.println("Hai kangaroo se khong gap nhau");
    }
}
