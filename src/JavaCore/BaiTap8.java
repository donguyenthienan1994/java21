package JavaCore;

import java.util.Scanner;

public class BaiTap8 {
    public static void main(String[] args) {
        Scanner scanner = null;
        System.out.print("Nhap chuoi ky tu: ");
        scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String[] array = str.split(" ");

        for (String s : array) {
            if(s.length() > 1)
                System.out.print(String.valueOf(s.charAt(0)).toUpperCase() + s.substring(1) + " ");
            else if (s.length() == 1)
                System.out.print(String.valueOf(s.charAt(0)).toUpperCase() + " ");
        }
    }
}
