package JavaCore;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class BaiTap9 {
    public static void main(String[] args) {
        Scanner scanner;
        long x = -1, y = -1;
        for (int i = 1; i <= 2; i++) {
            boolean isInputDone = false;
            do{
                try {
                    System.out.printf("Nhap so nguyen duong thu %d: ", i);
                    scanner = new Scanner(System.in);
                    int j = scanner.nextInt();
                    if (j < 0) throw new Exception("Xin hay nhap so nguyen duong");
                    if (String.valueOf(j).length() != 2) throw new Exception("Xin hay nhap so nguyen duong co 2 chu so");
                    if(i == 1){
                        x = j;
                    } else y = j;
                    isInputDone = true;
                } catch (InputMismatchException e){
                    System.out.println("Xin hay nhap so!!!!");
                    continue;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } while (!isInputDone);
        }

        boolean result = false;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                if(String.valueOf(x).toCharArray()[i] == String.valueOf(y).toCharArray()[j]){
                    result = true;
                    break;
                }
            }
        }

        System.out.println(!result);

    }
}
