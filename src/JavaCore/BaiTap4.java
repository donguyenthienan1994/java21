package JavaCore;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BaiTap4 {
    public static void main(String[] args) {
        Scanner scanner = null;
        long number = 0;
        do{
            try {
                System.out.print("Nhap so tu nhien: ");
                scanner = new Scanner(System.in);
                number = scanner.nextLong();
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so");
                continue;
            }
        } while (number == 0);
        int sum = 0;
        for (char c : String.valueOf(number).toCharArray()) {
            //convert char to number
//            int convert = c - '0';
            sum = sum + Integer.parseInt(String.valueOf(c));
        }
        System.out.println("Tong cac chu so la: " + sum);
    }
}
