package JavaCore;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class BaiTap18 {
    public static void main(String[] args) {
        Scanner scanner;
        boolean isInputDone = false;
        float inMoney = 0, outMoney = 0, rate = 0;
        //Nhap so tien
        do{
            try {
                System.out.print("Nhap so tien gui(VND): ");
                scanner = new Scanner(System.in);
                inMoney = scanner.nextFloat();
                System.out.print("Nhap so tien muon nhan duoc(VND): ");
                scanner = new Scanner(System.in);
                outMoney = scanner.nextFloat();
                System.out.print("Nhap lai suat(%/nam): ");
                scanner = new Scanner(System.in);
                rate = scanner.nextFloat();
                if(outMoney < inMoney){
                    System.out.println("So tien nhan duoc phai lon hon so tien gui");
                    continue;
                } else if(rate <= 0){
                    System.out.println("Lai suat phai > 0%");
                    continue;
                }
                isInputDone = true;
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so chinh xac");
                continue;
            }
        } while (!isInputDone);
        float monthlyMoney = (inMoney * rate)/(12 * 100);
        float savedMoney = outMoney - inMoney;
        float expectedNumberOfMonth = savedMoney/monthlyMoney;
        System.out.printf("Thoi gian phai cho la it nhat %d thang", Math.round(expectedNumberOfMonth));

    }
}
