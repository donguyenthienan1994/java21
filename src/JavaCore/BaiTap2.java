package JavaCore;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BaiTap2 {
    public static void main(String[] args) {

        long number = 0;
        do{
            try {
                System.out.print("Nhap so thap phan: ");
                Scanner scanner = new Scanner(System.in);
                number = scanner.nextLong();
            } catch (InputMismatchException e){
                System.out.println("Xin hay nhap so");
                continue;
            }
        } while (number == 0);

        int temp = 1;
        String binaryNumber = "";
        while (number != 0){
            long i = number % 2;
            number = number / 2;
            binaryNumber = i + binaryNumber;
        }
        System.out.println("So nhi phan la: " + binaryNumber);
    }
}
