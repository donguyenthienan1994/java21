package JavaCore;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class BaiTap5 {
    public static void main(String[] args) {
        Scanner scanner;
        Map map = new HashMap<String,Integer>();
        boolean isInputDone = false;
        for (int i = 1; i <= 2; i++) {
            do{
                try {
                    System.out.printf("Nhap toa do diem thu %d: ", i);
                    System.out.printf("\nx%d: ", i);
                    scanner = new Scanner(System.in);
                    int x = scanner.nextInt();
                    System.out.printf("y%d: ", i);
                    scanner = new Scanner(System.in);
                    int y = scanner.nextInt();
                    map.put(String.format("x%d", i), x);
                    map.put(String.format("y%d", i), y);
                    isInputDone = true;
                } catch (InputMismatchException e){
                    System.out.println("Xin hay nhap so!!!!");
                    continue;
                }

            } while (!isInputDone);
        }

        int x1 = Integer.parseInt(map.get("x1").toString());
        int y1 = Integer.parseInt(map.get("y1").toString());
        int x2 = Integer.parseInt(map.get("x2").toString());
        int y2 = Integer.parseInt(map.get("y2").toString());

        double length = Math.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2) * (y1 - y2));
        System.out.println("Do dai AB la : " + length);
    }
}
