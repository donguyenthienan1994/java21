package OOP.QLNS.main;

import OOP.QLNS.model.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class QLNS {
    private static Scanner scanner;
    private static List<NhanVien> nhanVienList = new ArrayList<>();
    public static void main(String[] args) throws Exception {
        ((GiamDoc)themNhanVien("Idol", ChucVu.GIAM_DOC).setSoNgayLamViec(22)).setSoLuongCoPhan(11);
        ((GiamDoc)themNhanVien("Den", ChucVu.GIAM_DOC).setSoNgayLamViec(22)).setSoLuongCoPhan(12);
        ((GiamDoc)themNhanVien("Bong", ChucVu.GIAM_DOC).setSoNgayLamViec(22)).setSoLuongCoPhan(13);
        ((GiamDoc)themNhanVien("Xang", ChucVu.GIAM_DOC).setSoNgayLamViec(22)).setSoLuongCoPhan(14);
        themNhanVien("Ngan", ChucVu.TRUONG_PHONG).setSoNgayLamViec(10);
        themNhanVien("Tuyen", ChucVu.TRUONG_PHONG).setSoNgayLamViec(10);
        themNhanVien("An", ChucVu.NHAN_VIEN_THUONG).setSoNgayLamViec(30);
        themNhanVien("Chi", ChucVu.NHAN_VIEN_THUONG).setSoNgayLamViec(30);
        //Phân bổ Nhân viên vào Trưởng phòng
        phanBoNhanVien("An","Ngan");
        phanBoNhanVien("Chi","Ngan");
        //Xuat luong ca cong ty
        xuatLuong();
        //Xuat nhan vien luong cao nhat
        xuatNhanVienLuongCaoNhat();
        //Xuat truong phong nhieu nhan vien nhat
        xuatTruongPhongNhieuNhanVienNhat();
        //Xoa truong phong
        xoaNhanVien("Ngan", ChucVu.TRUONG_PHONG);
        inNhanSu();
        //sap Xep nhan vien theo abc
        sapXepNhanVienTheoTen();
        sapXepNhanVienTheoLuong();
        xuatGiamDocCoCoPhanNhieuNhat();
    }

    private static void phanBoNhanVien(String tenNV, String tenTP){
        NhanVienThuong nhanVienThuong = (NhanVienThuong) nhanVienList.stream()
                .filter(nhanVien -> nhanVien.getTen().equalsIgnoreCase(tenNV)
                        && nhanVien.getChucVu() == ChucVu.NHAN_VIEN_THUONG).findAny().orElse(null);

        if(nhanVienThuong == null) throw new RuntimeException("Khong co nhan vien ten : " + tenNV);

        TruongPhong truongPhong = (TruongPhong) nhanVienList.stream()
                .filter(nhanVien -> nhanVien.getTen().equalsIgnoreCase(tenTP)
                        && nhanVien.getChucVu() == ChucVu.TRUONG_PHONG).findAny().orElse(null);

        if(truongPhong == null) throw new RuntimeException("Khong co truong phong ten : " + tenTP);

        nhanVienList.forEach(nhanVien -> {
            if(nhanVien.getTen().equalsIgnoreCase(tenTP) && nhanVien.getChucVu() == ChucVu.TRUONG_PHONG){
                ((TruongPhong) nhanVien).setSoLuongNhanVienDuoiQuyen();
                ((TruongPhong) nhanVien).phanBoNhanVien(nhanVienThuong);
            }
        });

        nhanVienList.forEach(nhanVien -> {
            if(nhanVien.getTen().equalsIgnoreCase(tenNV)){
                ((NhanVienThuong) nhanVien).setTruongPhongQuanLy(truongPhong);
            }
        });
    }

    private static NhanVien themNhanVien(String hoTen, ChucVu chucVu) throws Exception {
        NhanVien nhanVien = null;
        switch (chucVu){
            case NHAN_VIEN_THUONG:
                nhanVien = new NhanVienThuong(hoTen);
                break;
            case GIAM_DOC:
                nhanVien = new GiamDoc(hoTen);
                break;
            case TRUONG_PHONG:
                nhanVien = new TruongPhong(hoTen);
                break;
            default:
                throw new Exception("Khong co chuc vu : " + chucVu.getValue());
        }
        nhanVienList.add(nhanVien);
        return nhanVien;
    }

    private static void xoaNhanVien(String hoTen, ChucVu chucVu){
        NhanVien nhanVien = nhanVienList.stream()
                .filter(nv -> nv.getTen().equalsIgnoreCase(hoTen)
                        && nv.getChucVu() == chucVu).findAny().orElse(null);
        if(nhanVien == null) throw new RuntimeException(String.format("Khong co %s ten: %s%n", chucVu.getValue(), hoTen));

        if(nhanVien.getChucVu() == ChucVu.TRUONG_PHONG){
            ((TruongPhong) nhanVien).getListNhanVienDuoiQuyen(nhanVienList, hoTen)
                    .forEach(nhanVienDuoiQuyen -> ((NhanVienThuong) nhanVienDuoiQuyen).setTruongPhongQuanLy(null));
        }
        nhanVienList.removeIf(nv -> nv.getChucVu() == chucVu && nv.getTen().equalsIgnoreCase(hoTen));
    }

    private static void inNhanSu(){
        nhanVienList.forEach(nhanVien -> nhanVien.inThongTinCaNhan());
    }

    private static void xuatLuong(){
        nhanVienList.forEach(nhanVien -> nhanVien.xuatLuong());
    }

    private static void xuatNhanVienLuongCaoNhat(){
        NhanVien nvMax = nhanVienList.stream().max(Comparator.comparingDouble(NhanVien::tinhLuong)).get();
        System.out.printf("Nhan vien %s co luong cao nhat %s$",nvMax.getTen(), nvMax.tinhLuong());
    }

    private static void xuatTruongPhongNhieuNhanVienNhat(){
        ArrayList<TruongPhong> truongPhongList = new ArrayList<>();
        nhanVienList.stream().filter(tp -> tp.getChucVu() == ChucVu.TRUONG_PHONG)
                .collect(Collectors.toList()).forEach(nhanVien -> {
                    truongPhongList.add((TruongPhong) nhanVien);
                });
        TruongPhong truongPhongMax = truongPhongList.stream()
                .max(Comparator.comparingInt(TruongPhong::getSoLuongNhanVienDuoiQuyen)).get();

        System.out.printf("%nTruong phong %s co so luong nhan vien duoi quyen cao nhat %s",truongPhongMax.getTen(), ((TruongPhong)(truongPhongMax)).getSoLuongNhanVienDuoiQuyen());
    }

    private static void sapXepNhanVienTheoTen(){
        System.out.println("Sap xep nhan vien theo thu tu abc");
        nhanVienList.stream().sorted(Comparator.comparing(NhanVien::getTen))
                .forEach(nhanVien -> nhanVien.inThongTinCaNhan());
    }

    private static void sapXepNhanVienTheoLuong(){
        System.out.println("Sap xep nhan vien theo luong giam dan");
        nhanVienList.stream().sorted(Comparator.comparing(NhanVien::tinhLuong,Comparator.reverseOrder()))
                .forEach(nhanVien -> nhanVien.xuatLuong());
    }

    private static void xuatGiamDocCoCoPhanNhieuNhat(){
        ArrayList<GiamDoc> giamDocList = new ArrayList<>();
        nhanVienList.stream().filter(tp -> tp.getChucVu() == ChucVu.GIAM_DOC)
                .collect(Collectors.toList()).forEach(nhanVien -> {
                    giamDocList.add((GiamDoc) nhanVien);
                });
        GiamDoc giamDocMax = giamDocList.stream()
                .max(Comparator.comparingInt(GiamDoc::getSoLuongCoPhan)).get();

        System.out.printf("%nGiam doc %s co so luong co phan cao nhat %s",giamDocMax.getTen(), ((GiamDoc)(giamDocMax)).getSoLuongCoPhan());
    }
}
