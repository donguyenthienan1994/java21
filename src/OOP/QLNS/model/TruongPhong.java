package OOP.QLNS.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TruongPhong extends NhanVien{
    private int soLuongNhanVienDuoiQuyen = 0;
    private List<NhanVienThuong> nhanVienThuongList;

    public TruongPhong(){
        luong1Ngay = 200;
        setChucVu(ChucVu.TRUONG_PHONG);
        nhanVienThuongList = new ArrayList<>();
    }

    public TruongPhong(String hoTen){
        super(hoTen);
        luong1Ngay = 200;
        setChucVu(ChucVu.TRUONG_PHONG);
        nhanVienThuongList = new ArrayList<>();
    }

    public int getSoLuongNhanVienDuoiQuyen() {
        return soLuongNhanVienDuoiQuyen;
    }

    public void setSoLuongNhanVienDuoiQuyen() {
        this.soLuongNhanVienDuoiQuyen++;
    }

    public void phanBoNhanVien(NhanVienThuong nhanVien){
        nhanVienThuongList.add(nhanVien);
    }

    @Override
    public double tinhLuong() {
        return luong1Ngay * getSoNgayLamViec() + 100 * soLuongNhanVienDuoiQuyen;
    }

    @Override
    public void inThongTinCaNhan() {
        super.inThongTinCaNhan();
        System.out.printf(" - soLuongNhanVien: %d%n", soLuongNhanVienDuoiQuyen);
    }

    public List<NhanVien> getListNhanVienDuoiQuyen(List<NhanVien> nhanVienList, String tenTruongPhong){
        return nhanVienList.stream().filter(nhanVien -> nhanVien.getChucVu() == ChucVu.NHAN_VIEN_THUONG
                        && ((NhanVienThuong) nhanVien).getTruongPhongQuanLy().getTen().equalsIgnoreCase(tenTruongPhong))
                .collect(Collectors.toList());
    }
}
