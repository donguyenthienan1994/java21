package OOP.QLNS.model;

public class GiamDoc extends NhanVien{
    private int soLuongCoPhan;

    public int getSoLuongCoPhan() {
        return soLuongCoPhan;
    }

    public void setSoLuongCoPhan(int soLuongCoPhan) {
        if(soLuongCoPhan < 0 || soLuongCoPhan > 100){
            throw new RuntimeException("So luong co phan(%) phai tu 0-100");
        }
        this.soLuongCoPhan = soLuongCoPhan;
    }

    public GiamDoc(){
        super();
        luong1Ngay = 300;
        setChucVu(ChucVu.GIAM_DOC);
    }

    public GiamDoc(String hoTen){
        super(hoTen);
        luong1Ngay = 300;
        setChucVu(ChucVu.GIAM_DOC);
    }

    @Override
    public double tinhLuong() {
        return luong1Ngay * getSoNgayLamViec();
    }

    @Override
    public void inThongTinCaNhan() {
        super.inThongTinCaNhan();
        System.out.println();
    }
}
