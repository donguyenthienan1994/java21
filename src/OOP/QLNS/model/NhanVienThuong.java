package OOP.QLNS.model;

import java.util.Objects;

public class NhanVienThuong extends NhanVien{

    private TruongPhong truongPhongQuanLy;

    public TruongPhong getTruongPhongQuanLy() {
        return truongPhongQuanLy;
    }

    public void setTruongPhongQuanLy(TruongPhong truongPhongQuanLy) {
        this.truongPhongQuanLy = truongPhongQuanLy;
    }

    public NhanVienThuong(String hoTen){
        super(hoTen);
        luong1Ngay = 100;
        setChucVu(ChucVu.NHAN_VIEN_THUONG);
    }

    @Override
    public double tinhLuong() {
        return luong1Ngay * getSoNgayLamViec();
    }

    @Override
    public void inThongTinCaNhan() {
        super.inThongTinCaNhan();
        System.out.printf(" - Truong Phong: %s%n", Objects.isNull(truongPhongQuanLy) ? null : truongPhongQuanLy.getTen());
    }
}
