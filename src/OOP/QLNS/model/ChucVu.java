package OOP.QLNS.model;

public enum ChucVu {
    NHAN_VIEN_THUONG("Nhan vien thuong"), TRUONG_PHONG("Truong phong"), GIAM_DOC("Giam doc");

    private String value;
    ChucVu(String chucVu) {
        value = chucVu;
    }
    public String getValue(){
        return this.value;
    }
}
