package OOP.QLNS.model;


/*Diem khac nhau giua abstract class vs interface
//abtract : khai bao bien, xu ly logic code, khai bao ham. Mot class chi duoc ke thua 1 lan
//interface : dinh nghia ham, khai bao hang so. 1 class co the ke thua nhieu interface khac nhau
*/
public abstract class NhanVien implements ChucNang{
    private String maSoThue;
    private String hoten;
    private int sdt;
    private ChucVu chucVu;
    private int soNgayLamViec;
    protected double luong1Ngay;

    public NhanVien(String hoten){
        setTen(hoten);
    }

    public NhanVien() {

    }

    public void inThongTinCaNhan(){
        System.out.printf("%n" + getChucVu().getValue() + " - " + getTen());
    }

    public void xuatLuong(){
        inThongTinCaNhan();
        System.out.println(" - Luong: " + tinhLuong());
    }

    public int getSoNgayLamViec() {
        return soNgayLamViec;
    }

    public NhanVien setSoNgayLamViec(int soNgayLamViec) {
        this.soNgayLamViec = soNgayLamViec;
        return this;
    }

    public String getTen() {
        return hoten;
    }

    public void setTen(String hoten) {
        this.hoten = hoten;
    }

    public int getSdt() {
        return sdt;
    }

    public void setSdt(int sdt) {
        this.sdt = sdt;
    }

    public ChucVu getChucVu() {
        return chucVu;
    }

    public void setChucVu(ChucVu chucVu) {
        this.chucVu = chucVu;
    }
}
