package OOP.DemoSinhVienV1.model;

public class SinhVien {
    public String hoTen;
    private String maSV;
    private int diemToan;
    private int diemLy;
    private int diemHoa;
    private float diemTrungBinh;
    private String loai;

    public SinhVien(){}

    public float tinhDiemTrungBinh() {
        diemTrungBinh = (getDiemToan() + getDiemHoa() + getDiemToan()) / 3;
        return diemTrungBinh;
    }

    public String xepLoai() {
        float diemTB = tinhDiemTrungBinh();
        loai = "Yeu";
        if(diemTB >= 9){
            loai = "Xuat Sac";
        } else if(diemTB >= 8){
            loai = "Gioi";
        } else if(diemTB >= 7){
            loai = "Kha";
        } else if(diemTB >= 6){
            loai = "TB Kha";
        } else if(diemTB >= 5){
            loai = "Trung Binh";
        }
        return loai;
    }

    public SinhVien (String hoTen, String maSV){
        setHoTen(hoTen);
        setMaSV(maSV);
    }

    public int getDiemToan() {
        return diemToan;
    }

    public void setDiemToan(int diemToan) {
        this.diemToan = diemToan;
    }

    public int getDiemLy() {
        return diemLy;
    }

    public void setDiemLy(int diemLy) {
        this.diemLy = diemLy;
    }

    public int getDiemHoa() {
        return diemHoa;
    }

    public void setDiemHoa(int diemHoa) {
        this.diemHoa = diemHoa;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getMaSV() {
        return maSV;
    }

    public void setMaSV(String maSV) {
        this.maSV = maSV;
    }
}
