package OOP.DemoSinhVienV1.main;

import OOP.DemoSinhVienV1.model.SinhVien;

import java.util.Scanner;

public class DemoSinhVienV1 {
    private static Scanner scanner;
    public static void main(String[] args) {
        System.out.print("Nhap so luong sinh vien: ");
        int soLuongSV = refreshScanner().nextInt();
        SinhVien[] dsSinhVien = new SinhVien[soLuongSV];
        //Nhap so luong sinh vien
        for (int i = 1; i <= dsSinhVien.length; i++) {
            dsSinhVien[i - 1] = new SinhVien();
            System.out.printf("\nNhap ma so sinh vien thu %d: ", i);
            dsSinhVien[i - 1].setMaSV(refreshScanner().nextLine());
            System.out.printf("\nNhap ho ten sinh vien thu %d: ", i);
            dsSinhVien[i - 1].setHoTen(refreshScanner().nextLine());
        }

        //Nhap diem sinh vien
        for (SinhVien sinhVien : dsSinhVien) {
            System.out.printf("\nNhap diem Toan cua sinh vien '%s' : ", sinhVien.getHoTen());
            sinhVien.setDiemToan(refreshScanner().nextInt());
            System.out.printf("\nNhap diem Ly cua sinh vien '%s' : ", sinhVien.getHoTen());
            sinhVien.setDiemLy(refreshScanner().nextInt());
            System.out.printf("\nNhap diem Hoa cua sinh vien '%s' : ", sinhVien.getHoTen());
            sinhVien.setDiemHoa(refreshScanner().nextInt());
        }

        //Xep loai Sinh Vien
        for (SinhVien sinhVien : dsSinhVien){
            System.out.printf("\nSinh vien '%s' duoc xep loai '%s'!!!", sinhVien.getHoTen(), sinhVien.xepLoai());
        }
    }

    private static Scanner refreshScanner(){
        return scanner = new Scanner(System.in);
    }
}
